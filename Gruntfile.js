module.exports = function (grunt) {
    grunt.initConfig({
        eslint: {
            app: {
                src: [
                    'app/**/*.js',
                    '!app/**/*test.js'
                ]
            }
        },

        clean: {
            dist: {
                src: ['dist/']
            }
        },

        copy: {
            fonts: {
                expand: true,
                flatten: true,
                src: 'bower_components/bootstrap-sass/assets/fonts/bootstrap/*',
                dest: 'dist/fonts/bootstrap/'
            }
        },

        ngtemplates: {
            addressBookApp: {
                src: 'app/**/*.html',
                dest: 'dist/js/app.templates.js'
            }
        },

        browserify: {
            dist: {
                files: {
                    'dist/js/app.js': [
                        'app/**/*.js',
                        '!app/**/*test.js'
                    ]
                },
                options: {
                    browserifyOptions: {
                        debug: true
                    }
                }
            }
        },

        uglify: {
            dist: {
                files: {
                    'dist/js/app.min.js': ['dist/js/app.js', 'dist/js/app.templates.js']
                }
            }
        },

        compass: {
            dist: {
                options: {
                    outputStyle: 'compressed',
                    sassDir: 'assets/sass',
                    cssDir: 'dist/css',
                    fontsDir: 'dist/fonts',
                    environment: 'development',
                    httpPath: '../../app',
                    imagesDir: 'dist/img',
                    httpImagesPath: '../img',
                    httpGeneratedImagesPath: '../img'
                }
            }
        },

        watch: {
            scripts: {
                files: ['app/**/*.js'],
                tasks: ['browserify'],
                options: {
                    interrupt: true
                }
            },
            styles: {
                files: 'assets/sass/**/*.scss',
                tasks: ['compass'],
                options: {
                    interrupt: true
                }
            }
        }
    });

    grunt.loadNpmTasks('gruntify-eslint');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['eslint', 'clean', 'copy', 'ngtemplates', 'browserify', 'compass', 'watch']);
    grunt.registerTask('build', ['eslint', 'clean', 'copy', 'ngtemplates', 'browserify', 'uglify', 'compass']);
};