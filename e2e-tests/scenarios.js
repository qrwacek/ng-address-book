'use strict';

describe('address book app', function () {

    it('should automatically redirect to /list when location hash/fragment is empty', function () {
        browser.get('index.html');
        expect(browser.getLocationAbsUrl()).toMatch("/list");
    });

    describe('entries list view', function () {

        beforeEach(function () {
            browser.get('index.html#/list');
        });

        it('should render entries list when user navigates to /list', function () {
            expect(element.all(by.css('[data-ng-view] h2')).first().getText()).toMatch(/Entries list/);
        });

    });

    describe('new entry view', function () {

        beforeEach(function () {
            browser.get('index.html#/add');
        });

        it('should render entry add form when user navigates to /add', function () {
            expect(element.all(by.css('[data-ng-view] h2')).first().getText()).toMatch(/New entry/);
        });

        it('should return to entries list when "Cancel" button is used', function () {
            var cancelButton = element(by.linkText('Cancel'));
            cancelButton.click();

            expect(browser.getLocationAbsUrl()).toMatch("/list");
        });

        it('should validate email', function () {
            var emailInput = element(by.css('#email'));

            emailInput.sendKeys('not-an-email');
            expect(emailInput.getAttribute('class')).toMatch('ng-invalid-pattern')
        });

        it('should add entry and redirect to entries list ', function () {
            var firstNameInput = element(by.css('#first-name')),
                lastNameInput = element(by.css('#last-name')),
                emailInput = element(by.css('#email')),
                countrySelect = element(by.css('#country')),
                plOption = countrySelect.element(by.css('option[value="PL"]')),
                submitButton = element(by.css('[type="submit"]'));

            firstNameInput.sendKeys('Jan');
            lastNameInput.sendKeys('Kowalski');
            emailInput.sendKeys('jan.kowalski@example.com');
            plOption.click();
            submitButton.click();

            expect(browser.getLocationAbsUrl()).toMatch("/list");
            expect(element.all(by.css('tbody tr')).count()).toEqual(1);
        });

    });

    describe('entry details view', function () {

        beforeEach(function () {
            browser.get('index.html#/edit/1');
        });

        it('should display entry details', function () {
            var emailInput = element(by.css('#email'));

            expect(emailInput).toBeDefined();
            expect(emailInput.getAttribute('value')).toEqual('jan.kowalski@example.com');
        });

        it('should update entry details and redirect to entries list', function () {
            var emailInput = element(by.css('#email')),
                submitButton = element(by.css('[type="submit"]'));

            emailInput.clear();
            emailInput.sendKeys('jan.kowalski@test.pl');
            submitButton.click();

            expect(browser.getLocationAbsUrl()).toMatch("/list");
            expect(element.all(by.css('tbody tr td')).get(2).getText()).toEqual('jan.kowalski@test.pl');
        });

        it('should delete entry and redirect to entries list', function () {
            var deleteButton = element(by.buttonText('Delete'));
            deleteButton.click();

            expect(browser.getLocationAbsUrl()).toMatch("/list");
            expect(element.all(by.css('tbody tr')).count()).toEqual(0);
        });

    });

});
