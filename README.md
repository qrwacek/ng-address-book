# AngularJS Address Book 

Simple address book created using AngularJS. Data is persisted using localStorage.

## Getting Started

Clone the repository and install the dependencies.

### Install Dependencies

To install app dependencies run

```
npm install
```

Behind the scenes this will also call `bower install`.

### Build the Application

App uses [Grunt][gruntjs] task runner. To build the app simply run

```
grunt build
```

### Run the Application

There is a pre-configured simple development web server. To start the server run

```
npm start
```

Now browse to the app at [`localhost:8000/index.html`][local-app-url].


## Directory Layout

```
app/                  --> all of the source files for the application
  components/         --> all app specific modules
  shared/             --> all modules shared across the application
  app.js              --> main application module
assets/               --> sources for non-angular app assets
karma.conf.js         --> config file for running unit tests with Karma
e2e-tests/            --> end-to-end tests
  protractor-conf.js  --> Protractor config file
  scenarios.js        --> end-to-end scenarios to be run by Protractor
index.html            --> app layout file (the main html template file of the app)
```


## Testing

There are two kinds of tests in the application: Unit tests and end-to-end tests.

### Running Unit Tests

The app comes preconfigured with unit tests. These are written in [Jasmine][jasmine],
which is run with the [Karma][karma] test runner. There is a Karma configuration file to run them.

* The configuration is found at `karma.conf.js`.
* The unit tests are found next to the code they are testing and have an `-test.js` suffix.

To run the unit tests use the supplied npm script:

```
npm test
```

This script will start the Karma test runner to execute the unit tests. Moreover, Karma will start
watching the source and test files for changes and then re-run the tests whenever any of them
changes.

You can also ask Karma to do a single run of the tests and then exit. The project contains a
predefined script to do this:

```
npm run test-single-run
```

### Running End-to-End Tests

The app comes with end-to-end tests, written in [Jasmine][jasmine]. These tests
are run with the [Protractor][protractor] End-to-End test runner. 

* The configuration is found at `e2e-tests/protractor-conf.js`.
* The end-to-end tests are found in `e2e-tests/scenarios.js`.

Before starting Protractor, web server needs to be serving up the application. Open a separate terminal window and run:

```
npm start
```

Once the development web server hosting our application is up and running,
run the end-to-end tests using the supplied npm script:

```
npm run protractor
```

[angularjs]: https://angularjs.org/
[bower]: http://bower.io/
[git]: https://git-scm.com/
[http-server]: https://github.com/indexzero/http-server
[jasmine]: https://jasmine.github.io/
[jdk]: https://wikipedia.org/wiki/Java_Development_Kit
[jdk-download]: http://www.oracle.com/technetwork/java/javase/downloads
[karma]: https://karma-runner.github.io/
[local-app-url]: http://localhost:8000/index.html
[node]: https://nodejs.org/
[npm]: https://www.npmjs.org/
[protractor]: http://www.protractortest.org/
[selenium]: http://docs.seleniumhq.org/
[travis]: https://travis-ci.org/
[travis-docs]: https://docs.travis-ci.com/user/getting-started
[gruntjs]: http://gruntjs.com/getting-started
