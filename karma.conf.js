//jshint strict: false
module.exports = function (config) {
    config.set({

        files: [
            'dist/js/app.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'app/**/*test.js'
        ],

        autoWatch: true,

        frameworks: ['jasmine'],

        browsers: ['Chrome'],

        plugins: [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
        ],

        junitReporter: {
            outputFile: 'test_out/unit.xml',
            suite: 'unit'
        }

    });
};
