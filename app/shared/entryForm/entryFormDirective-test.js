'use strict';

describe('addressBookApp.entryForm module', function() {
    beforeEach(module('addressBookApp.entryForm'));

    describe('entry-form directive', function() {
        it('should print entry form', function() {
            inject(function($compile, $rootScope) {
                var element = $compile('<entry-form></entry-form>')($rootScope);
                expect(element.find('[name="entryForm"]')).toBeDefined();
            });
        });
    });
});
