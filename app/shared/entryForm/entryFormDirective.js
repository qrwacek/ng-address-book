'use strict';

angular.module('addressBookApp.entryForm', ['addressBookApp.countries'])

    .directive('entryForm', [function () {
        return {
            templateUrl: 'app/shared/entryForm/entryFormDirective.html',
            controller: ['$scope', 'countriesService', function ($scope, countriesService) {
                $scope.countries = countriesService.getData();
                // eslint-disable-next-line max-len
                $scope.email_regexp = '^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&\'*+/0-9=?A-Z^_`a-z{|}~]+(\\.[-!#$%&\'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$';
            }]
        }
    }]);