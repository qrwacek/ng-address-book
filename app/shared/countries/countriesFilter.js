'use strict';

angular.module('addressBookApp.countries.countries-filter', ['addressBookApp.countries.countries-service'])

    .filter('countryCodeToName', ['countriesService', function (countriesService) {
        return function (text) {
            return countriesService.getName(text);
        };
    }]);
