'use strict';

angular.module('addressBookApp.countries', [
    'addressBookApp.countries.countries-filter',
    'addressBookApp.countries.countries-service'
]);
