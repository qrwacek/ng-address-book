'use strict';

var countryList = require('country-list')();

angular.module('addressBookApp.countries.countries-service', [])

    .service('countriesService', [function () {
        this.getData = countryList.getData;
        this.getName = countryList.getName;
    }]);
