'use strict';

describe('addressBookApp.countries.countries-service module', function() {
    beforeEach(module('addressBookApp.countries.countries-service'));

    describe('countries service', function() {

        it('should return country list', inject(function (countriesService) {
            var countries = countriesService.getData();
            expect(countries.length).toBeGreaterThan(0);
            expect(countries[0].code).toBe('AF');
        }));

    });
});