'use strict';

describe('addressBookApp.countries.countries-filter module', function() {
    beforeEach(module('addressBookApp.countries.countries-filter'));

    describe('countries filter', function() {
        it('should replace country code with country name', inject(function(countryCodeToNameFilter) {
            expect(countryCodeToNameFilter('PL')).toEqual('Poland');
        }));
    });
});