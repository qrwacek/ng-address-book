'use strict';

describe('addressBookApp.entries.entries-service module', function() {
    beforeEach(module('addressBookApp.entries.entries-service'));

    describe('entries service', function() {
        var testEntry = {first_name: 'Jan', last_name: 'Kowalski', email: 'jan.kowalski@example.com', country: 'PL'};
        var EntryService;

        beforeEach(inject(function (_EntryService_) {
            EntryService = _EntryService_;
            EntryService.add(testEntry);
        }));

        afterEach(function () {
            EntryService.remove(1);
        });

        it('should return entries list', function () {
            var entries = EntryService.getAll();
            expect(entries).toEqual([testEntry]);
        });

    });
});