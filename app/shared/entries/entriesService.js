'use strict';

angular.module('addressBookApp.entries.entries-service', ['LocalStorageModule'])

    .service('EntryService', ['localStorageService', function (localStorageService) {
        var storageKey = 'addressBookEntries';
        var entries = [];

        load();

        function load() {
            entries = localStorageService.get(storageKey) || [];
        }
        function save() {
            localStorageService.set(storageKey, entries);
        }

        this.getAll = function (forceLoad) {
            if (forceLoad) {
                load();
            }
            return angular.copy(entries);
        };

        this.getById = function (id, getReference) {
            for (var i = 0; i < entries.length; i++) {
                if (entries[i].id == id) {
                    return getReference ? entries[i] : angular.copy(entries[i]);
                }
            }
            return null;
        };

        this.getLastInsertId = function () {
            return entries.length ? entries[entries.length - 1].id : 0;
        };

        this.add = function (entry) {
            entry.id = this.getLastInsertId() + 1;
            entries.push(entry);
            save();
        };

        this.update = function (entry) {
            var curr = this.getById(entry.id, true);
            if (curr) {
                angular.extend(curr, entry);
                save();
                return true;
            }
            return false;
        };

        this.remove = function (id) {
            for (var i = 0; i < entries.length; i++) {
                if (entries[i].id == id) {
                    entries.splice(i, 1);
                    save();
                    return true;
                }
            }
            return false;
        };
    }]);
