'use strict';

describe('addressBookApp.entryDetails module', function () {

    beforeEach(module('addressBookApp.entryDetails'));

    describe('entry details controller', function () {

        var testEntry = {id: 1, first_name: 'Jan', last_name: 'Kowalski', country: 'PL'};
        var scope, entryDetailsController;

        beforeEach(inject(function ($rootScope, $controller, EntryService, $routeParams) {
            $routeParams.id = '1';
            scope = $rootScope.$new();
            EntryService.add(testEntry);
            entryDetailsController = $controller('EntryDetailsController', {$scope: scope});
        }));

        afterEach(inject(function (EntryService) {
            EntryService.remove(testEntry.id);
        }));

        it('should load entry details', function () {
            expect(scope.entry).toEqual(testEntry);
        });

    });
});