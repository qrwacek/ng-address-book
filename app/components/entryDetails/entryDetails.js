'use strict';

angular.module('addressBookApp.entryDetails', [
    'ngRoute',
    'addressBookApp.entryForm',
    'addressBookApp.entries'
])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/edit/:id', {
            templateUrl: 'app/components/entryDetails/entryDetails.html',
            controller: 'EntryDetailsController'
        });
    }])

    .controller('EntryDetailsController', [
        '$scope', '$location', 'EntryService', '$routeParams',
        function ($scope, $location, EntryService, $routeParams) {
            $scope.entry = EntryService.getById($routeParams.id);

            $scope.saveEntry = function (entry) {
                if (EntryService.update(entry)) {
                    $location.path("/list");
                } else {
                    console.error('Error while updating entry');
                }
            };

            $scope.removeEntry = function (id) {
                EntryService.remove(id);
                $location.path("/list");
            };
        }
    ]);