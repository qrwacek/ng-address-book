'use strict';

describe('addressBookApp.entryAdd module', function () {

    beforeEach(module('addressBookApp.entryAdd'));

    describe('entry add controller', function () {

        var scope, entryAddController;

        beforeEach(inject(function ($rootScope, $controller) {
            scope = $rootScope.$new();

            entryAddController = $controller('EntryAddController', {$scope: scope});
        }));

        it('should ....', function () {
            expect(entryAddController).toBeDefined();
        });

    });
});
