'use strict';

angular.module('addressBookApp.entryAdd', [
    'ngRoute',
    'addressBookApp.entryForm',
    'addressBookApp.entries'
])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/add', {
            templateUrl: 'app/components/entryAdd/entryAdd.html',
            controller: 'EntryAddController'
        });
    }])

    .controller('EntryAddController', ['$scope', '$location', 'EntryService', function ($scope, $location, EntryService) {

        $scope.saveEntry = function (entry) {
            EntryService.add(entry);
            $location.path("/list");
        };

    }]);