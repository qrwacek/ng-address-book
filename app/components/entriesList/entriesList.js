'use strict';

angular.module('addressBookApp.entriesList', ['ngRoute', 'addressBookApp.entries'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/list', {
            templateUrl: 'app/components/entriesList/entriesList.html',
            controller: 'EntriesListController'
        });
    }])

    .controller('EntriesListController', ['$scope', 'EntryService', function ($scope, EntryService) {
        $scope.entries = EntryService.getAll();
    }]);
