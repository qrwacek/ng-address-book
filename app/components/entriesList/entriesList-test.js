'use strict';

describe('addressBookApp.entriesList module', function () {

    beforeEach(module('addressBookApp.entriesList'));

    describe('entries list controller', function () {

        var scope, entriesListController;

        beforeEach(inject(function ($rootScope, $controller, EntryService) {
            scope = $rootScope.$new();
            EntryService.add({id: 1, first_name: 'Jan', last_name: 'Kowalski', country: 'PL'});
            entriesListController = $controller('EntriesListController', {$scope: scope});
        }));

        afterEach(inject(function (EntryService) {
            EntryService.remove(1);
        }));

        it('should load entries', function () {
            expect(scope.entries.length).toEqual(1);
        });

    });
});