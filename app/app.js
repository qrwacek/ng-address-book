'use strict';

require('../bower_components/angular');
require('../bower_components/angular-route');
require('../bower_components/angular-local-storage');

angular.module('addressBookApp', [
    'ngRoute',
    'addressBookApp.entriesList',
    'addressBookApp.entryAdd',
    'addressBookApp.entryDetails'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $routeProvider.otherwise({redirectTo: '/list'});
}]);
